# Interoperability

This page will be extended as soon as there are more releases of the related repositories to inidicate which versions can be used together. Currently, only the latest releases of the `Wallet`, `Control Plane`, and `HTTP Data Plane` are expected to be interoperable, given the amount of changes being made to all of the repositories.

## External specifications

The currently used external specifications for this wallet:

- [Decentralized Identifiers (DIDs) v1.0](https://www.w3.org/TR/did-core/)
- [Verifiable Credentials Data Model v2.0](https://www.w3.org/TR/vc-data-model-2.0/)
- [Presentation Exchange 2.0.0](https://identity.foundation/presentation-exchange/spec/v2.0.0)
- [OpenID for Verifiable Credential Issuance - draft 13](https://openid.net/specs/openid-4-verifiable-credential-issuance-1_0.html)
- [Self-Issued OpenID Provider v2 - draft 13](https://openid.net/specs/openid-connect-self-issued-v2-1_0.html)
